---
title: IT Berater mit Schwerpunkt IoT & Open Region
subtitle: Offene Plattformen, Open Data und Bürgerbeteiligung
date: 2015-03-30T17:00:00+02:00
bigimg:
- src: "/img/path.jpg"
  desc: Path

---
Ich helfe bei den Herausforderungen der Digitalisierung durch Einsatz von Open Source und Kooperativen Plattformen um das beste für alle Beteiligten zu erreichen.

Digitalisierung fängt weit vor der Oberfläche an und bedeutet ein neue Denken der vorhandenen Prozesse